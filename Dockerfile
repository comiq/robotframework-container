FROM alpine:latest

RUN apk update
RUN apk add --no-cache \
    dbus \
    ttf-freefont \
    firefox-esr \
    chromium \
    chromium-chromedriver \
    python \
    py-pip \
    wget \
    tk-dev \
    python2-tkinter \
    git \
    udev \
    mesa-gl \
    mesa-dri-swrast

RUN pip install \
    robotframework==3.0 \
    selenium==2.53.6 \
    robotframework-selenium2library==1.8.0 \
    robotframework-databaselibrary

ENV DISPLAY=xvfb:0

WORKDIR /
COPY pybot.sh /usr/local/bin/
RUN chmod ugo+x /usr/local/bin/pybot.sh
RUN addgroup -g 1000 robotframework
RUN adduser -u 1000 -D -G robotframework robotframework
USER robotframework
ENTRYPOINT ["/usr/local/bin/pybot.sh"]
CMD ["--help"]
